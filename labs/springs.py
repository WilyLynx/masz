import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from matplotlib.animation import FuncAnimation


class Point:
    def __init__(self, x, y):
        self.pos = np.array([x, y])
        self.vel = np.array([0., 0.])
        self.force = np.array([0., 0.])

    def clear_force(self):
        self.force = np.array([0., 0.])

    def clear_velocity(self):
        self.vel = np.array([0., 0.])

    def loss_velocity(self):
        self.vel *= 0.95

    def apply_force(self, dt):
        self.vel += self.force * dt

    def calculate_new_pos(self, dt):
        self.pos += self.vel * dt


class Spring:
    DEF_LEN = 1.
    K = 1e-3

    def __init__(self, left: Point, right: Point):
        self.left = left
        self.right = right

    def calculate_force(self):
        dx = self.DEF_LEN - (self.left.pos - self.right.pos)
        F = - self.K * dx
        self.left.force -= F
        self.right.force += F


class SpringSystem:
    D_TIME = 1

    def __init__(self, vertices, springs):
        self.points = vertices
        self.springs = springs
        self.fig = plt.figure()
        ax = plt.axes(xlim=(-1, 2), ylim=(-1, 2))
        self.points_plot, = ax.plot([], [], 'bo')
        self.edges_plot, = ax.plot([], [], lw=1)

    @staticmethod
    def from_graph(G: nx.Graph):
        vertices2points = {v: Point(np.random.random(), np.random.random()) for v in G.nodes}
        springs = []
        for e in G.edges:
            springs.append(Spring(vertices2points[e[0]], vertices2points[e[1]]))
        return SpringSystem(vertices2points.values(), springs)

    def init_anim(self):
        self.points_plot.set_data([], [])
        self.edges_plot.set_data([], [])
        return self.points_plot, self.edges_plot,

    def get_X_Y_points(self):
        X = [p.pos[0] for p in self.points]
        Y = [p.pos[1] for p in self.points]
        return X, Y

    def get_edges_X_Y(self):
        X, Y = [], []
        for e in self.springs:
            x1, y1 = e.left.pos[0], e.left.pos[1]
            x2, y2 = e.right.pos[0], e.right.pos[1]
            X.append([x1, x2])
            Y.append([y1, y2])
        return X, Y

    def animate(self, i):
        for e in self.springs:
            e.calculate_force()
        for p in self.points:
            p.apply_force(self.D_TIME)
            p.calculate_new_pos(self.D_TIME)
            p.clear_force()
            p.loss_velocity()
        X, Y = self.get_X_Y_points()
        self.points_plot.set_data(X, Y)
        X_edges, Y_edges = self.get_edges_X_Y()
        self.edges_plot.set_data(X_edges, Y_edges)
        return self.points_plot, self.edges_plot

    def generate_animation(self):
        anim = FuncAnimation(self.fig, self.animate, init_func=self.init_anim,
                             frames=100, interval=20, blit=True)
        anim.save('animation/graph_spring.gif', writer='imagemagick')


G = nx.Graph()
G.add_edge(0, 1)
G.add_edge(1, 2)
G.add_edge(2, 3)
G.add_edge(3, 0)
G.add_edge(2, 1)

sys = SpringSystem.from_graph(G)
sys.generate_animation()
