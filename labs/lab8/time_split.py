import math
from ast import literal_eval

import networkx as nx
import numpy as np
from bokeh.models import Circle, MultiLine, Slider
from bokeh.palettes import inferno
from bokeh.plotting import figure, from_networkx
from bokeh.tile_providers import get_provider

import json

from bokeh.layouts import row, column
from bokeh.models import HoverTool, CheckboxButtonGroup
from bokeh.plotting import curdoc


def remove_nodes_without_position(graph, stops):
    nodes_idxs = set([s['id'] for s in stops])
    graph.remove_nodes_from([s for s in graph.nodes if s not in nodes_idxs])
    return graph


def remove_nodes_with_degree_zero(graph):
    return graph.subgraph([v for v in graph.nodes if graph.degree(v) > 0])


def merc_from_str(Coords):
    Coordinates = literal_eval(Coords)
    lat = Coordinates[0]
    lon = Coordinates[1]
    merc((lat, lon))


def merc(cords):
    lat = cords[0]
    lon = cords[1]

    r_major = 6378137.000
    x = r_major * math.radians(lon)
    scale = x / lon
    y = 180.0 / math.pi * math.log(math.tan(math.pi / 4.0 +
                                            lat * (math.pi / 180.0) / 2.0)) * scale
    return (x, y)


def generate_warsaw_plot(x_range=None, y_range=None):
    warsaw_cords = (52.23188090380439, 21.020714925242196)
    warsaw_merc = merc(warsaw_cords)
    PLT_X, PLT_Y = 1200, 900

    DIFF = 30_000

    ratio = PLT_X / PLT_Y
    diff_x = DIFF * ratio
    diff_y = DIFF / ratio
    if not x_range or not y_range:
        x_range = (warsaw_merc[0] - diff_x, warsaw_merc[0] + diff_x)
        y_range = (warsaw_merc[1] - diff_y, warsaw_merc[1] + diff_y)

    p = figure(plot_width=PLT_X, plot_height=PLT_Y,
               x_range=x_range, y_range=y_range,
               x_axis_type="mercator", y_axis_type="mercator",
               name='map')
    p.add_tile(get_provider('CARTODBPOSITRON'))
    return p


def add_plotting_attributes(graph, max_dgr):
    color_scheme = inferno(256)
    ALPHA_MIN, ALPHA_MAX = 0.2, 1.0
    edge_color = {}
    edge_alpha = {}
    for u, v, a in graph.edges(data=True):
        importance = np.log(a['weight']) / np.log(max_dgr)
        color_idx = int(importance * len(color_scheme))
        color_idx = min(len(color_scheme) - 1, color_idx)
        edge_color[(u, v)] = color_scheme[color_idx]
        edge_alpha[(u, v)] = importance * (ALPHA_MAX - ALPHA_MIN) + ALPHA_MIN

    nx.set_edge_attributes(graph, edge_color, "edge_color")
    nx.set_edge_attributes(graph, edge_alpha, "edge_alpha")
    return graph


def get_graph_renderer(graph):
    graph_layout = {s['id']: list(merc((s['y'], s['x']))) for s in stops}
    graph_renderer = from_networkx(graph, graph_layout)

    graph_renderer.edge_renderer.glyph = MultiLine(line_color='edge_color', line_alpha='edge_alpha', line_width=2)
    graph_renderer.node_renderer.glyph = Circle(size=3, fill_alpha=0.4)
    return graph_renderer


#########################################################################################
#########################################################################################


with open('../../data/ZTM/layers/stops.json', 'r', encoding='utf-8') as f:
    stops = json.load(f)

with open('../../data/ZTM/layers/time_edges.json', 'r', encoding='utf-8') as f:
    edges = json.load(f)

transport_types = ['bus', 'train', 'tram']
hours = [i for i in range(24)]
ZTM = {t: [nx.DiGraph(name=f'{t}_{h}') for h in hours] for t in transport_types}

for t, g_hours in ZTM.items():
    for i, g in enumerate(g_hours):
        g.add_nodes_from([(s['id'], s) for s in stops])
        hour_edges = [edg['graphs'] for edg in edges if edg['hour'] == i][0]
        type_edges = [e for e in hour_edges if e['transport'] == t]
        if len(type_edges) > 0:
            g.add_edges_from([(e['from'], e['to'], {'weight': e['weight']}) for e in type_edges[0]['edge']])
            g = remove_nodes_without_position(g, stops)
            g = remove_nodes_with_degree_zero(g)
            g_hours[i] = g

renderers = {}
p = generate_warsaw_plot()
for t, g_hours in ZTM.items():
    max_dgr = max([max([d[2]['weight'] for d in g.edges(data=True)], default=0) for g in ZTM[t]])
    renderers[t] = {}
    for i, g in enumerate(g_hours):
        g = add_plotting_attributes(g, max_dgr)
        graph_renderer = get_graph_renderer(g)
        graph_renderer.visible = False
        renderers[t][i] = graph_renderer
        p.renderers.append(graph_renderer)
p.add_tools(HoverTool(tooltips=[("id", "@id"), ("name", "@name")]))


class State:
    def __init__(self, init_h, checked_idxs, renderers, transport_types):
        self.h = init_h
        self.checked_idxs = set(checked_idxs)
        self.renderers = renderers
        self.transport_types = transport_types
        self.update_renderers_hours(init_h)
        for i in checked_idxs:
            self.renderers[transport_types[i]][self.h].visible = True

    def update_renderers_hours(self, h):
        if h != self.h:
            for i in self.checked_idxs:
                self.renderers[self.transport_types[i]][self.h].visible = False
                self.renderers[self.transport_types[i]][h].visible = True
            self.h = h

    def update_renderers_types(self, changed_box_idx):
        if changed_box_idx in self.checked_idxs:
            self.renderers[self.transport_types[changed_box_idx]][self.h].visible = False
            self.checked_idxs.remove(changed_box_idx)
        else:
            self.renderers[self.transport_types[changed_box_idx]][self.h].visible = True
            self.checked_idxs.add(changed_box_idx)


def update_types(attr, old, new):
    last_clicked_ID = list(set(old) ^ set(new))[0]
    sim_state.update_renderers_types(last_clicked_ID)


def update_hours(attr, old, new):
    sim_state.update_renderers_hours(new)


init_h = 0
checked_idxs = []
sim_state = State(init_h, checked_idxs, renderers, transport_types)

LABELS = ["Bus", "Train", "Tram"]
btn_grp = CheckboxButtonGroup(labels=LABELS, active=checked_idxs)
btn_grp.on_change('active', update_types)
hour_slider = Slider(start=0, end=23, value=0, step=1, title="Hour")
hour_slider.on_change('value', update_hours)

curdoc().add_root(row(p, column(btn_grp, hour_slider)))
