import networkx as nx
import numpy as np
import pandas as pd
from IPython.core import display as ipy_display
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression


def estimate_alpha(data):
    def _get_bins_middle(bins):
        return (bins[:-1] + bins[1:]) / 2
    MIN, MAX = 1, max(data)
    n_bins = 200
    X_bins = np.logspace(np.log(MIN), np.log(MAX), n_bins)
    counts, bins, _ = plt.hist(data, bins=X_bins, log=True, density=True)
    plt.close()
    X = _get_bins_middle(bins)
    X, Y = zip(*[(np.log(b), np.log(c)) for b, c in zip(X, counts) if c > 0])
    lin_reg = LinearRegression()
    lin_reg.fit(np.array(X).reshape(-1, 1), np.array(Y).reshape(-1, 1))
    return -(lin_reg.coef_[0][0])


def estimate_graph_alpha(graph: nx.Graph):
    dgrs = [d[1] for d in graph.degree]
    return estimate_alpha(dgrs)


def get_basic_parameters_table(graphs):
    """
    Generates table of basics graph parameters for given list.
    :param graphs: list of graphs
    :return: pandas table
    """
    records = []
    for g in graphs:
        records.append([
            g.name,
            g.is_directed(),
            len(g.nodes),
            len(g.edges),
            round(np.mean([d for v, d in g.degree]), 2),
            round(estimate_graph_alpha(g), 2),
            round(nx.algorithms.degree_assortativity_coefficient(g), 2)
        ])
    return pd.DataFrame(np.array(records),
                        columns=['name', 'direct', 'N', 'E', '<k>', 'alpha', 'r'])


def _column_styler(col):
    # It works, does it?
    if col.dtype in [int, float]:
        align = "right"
    else:
        align = "left"

    return ["text-align: %s" % align] * len(col)


def display_pandas_table(pd_table):
    """
    Print formatted pandas table
    :param pd_table: table to print
    :return: None
    """
    pd_table.style.apply(_column_styler)
    ipy_display.display_pretty(pd_table)
