import networkx as nx
import matplotlib.pyplot as plt


def set_white_ticks():
    """
    Sets white ticks on plt figure. Use for dark IDE background.
    :return: None
    """
    plt.tick_params(axis='x', which='both', colors='white')
    plt.tick_params(axis='y', which='both', colors='white')


def get_context_for_dark_IDE():
    """
    Returns plot contex with white face colour
    usage:
    with get_context_for_dark_IDE():
        plt.plot([1,2,3],[1,4,9])
    :return: plot context
    """
    return plt.rc_context({'figure.facecolor':'white'})


def pretty_draw(G: nx.Graph,
                layout=nx.kamada_kawai_layout,
                figsize=(12, 8),
                ax=None,
                **kwds):
    """
    Draw graph in nice format. Scales nodes sizes with degree. \n
    Available drawing layouts <https://networkx.org/documentation/stable/reference/drawing.html#module-networkx.drawing.layout>

    :param G: graph to draw
    :param layout: one of networkx available drawing layouts
    :param figsize: used if ax is None to create figure
    :param ax: pre-created figure ax
    :param kwds: other parameters for nx.draw_networkx_nodes and nx.draw_networkx_edges
    :return: None
    """
    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    dgrs = dict(G.degree)
    pos = layout(G)
    nx.draw_networkx_nodes(G, pos=pos, ax=ax, node_size=[v * 10 for v in dgrs.values()], alpha=0.9, **kwds)
    nx.draw_networkx_edges(G, pos=pos, ax=ax, alpha=0.2, **kwds)


def draw_degree_histogram(G: nx.Graph, figsize=(12, 8), ax=None, white_ticks=True):
    """
    Draw histogram of nodes degrees
    :param G: input graph
    :param figsize: used if ax is None
    :param ax: pre-created figure ax
    :return: None
    """
    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    dgrs = dict(G.degree)
    ax.hist(dgrs.values())
    if white_ticks:
        set_white_ticks()


def draw_graph_with_degree_histogram(G: nx.Graph,
                                    graph_layout=nx.kamada_kawai_layout,
                                    figsize=(16, 8),
                                     white_ticks=True):
    """
    Draw graph and degree histogram side by side. Adds information about nodes and edges count.
    :param G: input graph
    :param graph_layout: layout for graph drawing
    :param figsize: figure size
    :return: None
    """
    print('Nodes: ', len(G.nodes))
    print('Edges: ', len(G.edges))
    fig, axs = plt.subplots(1, 2, figsize=figsize)
    pretty_draw(G, graph_layout, ax=axs[0])
    draw_degree_histogram(G, ax=axs[1], white_ticks=white_ticks)
