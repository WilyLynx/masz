import networkx as nx


def get_graph_name_from_file(path):
    """
    Extracts graph name from file
    :param path: path to file
    :return: graph name
    """
    return path.split('/')[-1].split('.')[0]


def read_got_graph(path):
    """
    Reads graph from GameOfThrones format <https://networkofthrones.wordpress.com/>

    :param path: path to file
    :return: read graph
    """
    with open(path, 'r') as f:
        next(f, None)
        G = nx.parse_edgelist(f, comments='t', delimiter=',', create_using=nx.Graph(),
                              nodetype=str, data=(('weight', int), ('season', int)))
        G.name = get_graph_name_from_file(path)
    return G


def read_mtx(path, data=None):
    """
    Reads graph from MTX format. First lines of a file are expected to be a comment.\n
    Line format: from to <data>

    :param path: path to file
    :param data: additional fields for node, eg: (('weight', float), )
    :return: read graph
    """
    with open(path, 'r') as f:
        line = f.readline()
        while line[0] == '%':
            line = f.readline()
        G = nx.parse_edgelist(f, comments='t', delimiter=' ', create_using=nx.Graph(),
                              nodetype=int, data=data)
    G.name = get_graph_name_from_file(path)
    return G


def read_edges_with_weight(path):
    """
    Read graph from list of edges with weights \n
    Line format: from to weight

    :param path: path to file
    :return: read graph
    """
    with open(path, 'r') as f:
        G = nx.parse_edgelist(f, comments='t', delimiter=' ', create_using=nx.Graph(),
                              nodetype=int, data=(('weight', float),))
    G.name = get_graph_name_from_file(path)
    return G


def read_from_csv(path, skip_header=True):
    """
    Read graph from csv format
    :param path: path to file
    :param skip_header: skip if file contains header
    :return: read graph
    """
    with open(path, 'r') as f:
        if skip_header:
            next(f, None)
        G = nx.parse_edgelist(f, comments='t', delimiter=',', create_using=nx.Graph(),
                                 nodetype=int)
        G.name = get_graph_name_from_file(path)
        return G
