# MASZ
## Usage
Creating new conda environment
```
conda create --name MASZ --file conda_requirements.txt
```

OR 

Creating virtual environment 

Windows:
```
python3 -m venv MASZ_env
MASZ_env\bin\activate\activate.bat
pip install -r requirements.txt
```
Linux / MaacOS:
```
python3 -m venv MASZ_env
source MASZ_env/bin/activate
pip install -r requirements.txt
```
