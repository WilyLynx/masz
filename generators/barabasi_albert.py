import networkx as nx
import numpy as np


class BA_Generator:
    AVAILABLE_METHODS = ['original', 'A', 'B']

    def __init__(self, init_nodes: int, final_nodes: int, m: int):
        self._m = m
        self._final_nodes = final_nodes
        self._init_nodes = init_nodes
        self._G = None

    def get_original(self):
        self._G = nx.generators.complete_graph(self._init_nodes)
        return self._BA_algorithm(self._preference_distribution_for_original, add_node=False)

    def get_model_A(self):
        self._G = nx.generators.complete_graph(self._init_nodes)
        return self._BA_algorithm(self._uniform_distribution_node, add_node=True)

    def exec_step(self, v, method):
        if method == 'original':
            self._update_graph(v, self._m, self._preference_distribution_for_original, add_node=True)
        elif method == 'A':
            self._update_graph(v, self._m, self._uniform_distribution_node, add_node=True)
        elif method == 'B':
            for _ in range(self._m):
                v = self._preference_distribution_for_model_B()
                self._add_random_edge(v, self._preference_distribution_for_model_B)
        else:
            print(f"Wrong method. Chose from: {self.AVAILABLE_METHODS}")

    def set_G(self, graph: nx.Graph):
        self._G = graph

    def _update_graph(self, v, m, get_random_node, add_node):
        if add_node:
            self._G.add_node(v)
        for _ in range(m):
            self._add_random_edge(v, get_random_node)

    def _BA_algorithm(self, random_sampling_method, add_node) -> nx.Graph:
        for v in range(self._init_nodes, self._final_nodes):
            self._update_graph(v, self._m, random_sampling_method, add_node)
        return self._G

    def _add_random_edge(self, new_node: int, get_random_node):
        max_try = 3_000
        n = 0
        while True:
            n += 1
            BA_random_node = get_random_node()
            new_edge = (new_node, BA_random_node)
            if new_edge not in self._G.edges and new_node != BA_random_node:
                break
            if n > max_try:
                raise Exception('max node sampling exceeded in _add_random_edge. Node num ', new_node)
        self._G.add_edge(new_node, BA_random_node)

    def _preference_distribution(self, dgrs: dict):
        sum_dgrs = sum(dgrs.values())
        nodes_p = [d / sum_dgrs for d in dgrs.values()]
        return np.random.choice(self._G.nodes(), p=nodes_p)

    def _preference_distribution_for_original(self):
        dgrs = dict(self._G.degree)
        return self._preference_distribution(dgrs)

    def _uniform_distribution_node(self):
        return np.random.choice(self._G.nodes())

    def _preference_distribution_for_model_B(self):
        dgrs = dict(self._G.degree)
        dgrs = dict(map(lambda x: (x[0], x[1] + 1), dgrs.items()))
        return self._preference_distribution(dgrs)


